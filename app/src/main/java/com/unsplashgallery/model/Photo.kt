package com.unsplashgallery.model

import java.io.Serializable

data class Photo(
    var imageId: Int = 0,
    var imageUrl: String = "",
    var imageTitle: String = "",
    var imageAuthor: String = ""
)