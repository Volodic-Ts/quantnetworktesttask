package com.unsplashgallery.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.unsplashgallery.BuildConfig
import com.unsplashgallery.databinding.FragmentGalleryBinding
import com.unsplashgallery.helpers.ImageGalleryAdapter
import com.unsplashgallery.model.Photo
import org.json.JSONException
import org.json.JSONObject


class GalleryFragment : Fragment() {

    private var photosList: ArrayList<Photo> = ArrayList()
    private lateinit var mQueue: RequestQueue
    private lateinit var recyclerView: RecyclerView
    private lateinit var imageGalleryAdapter: ImageGalleryAdapter

    private var _binding: FragmentGalleryBinding? = null
    private val binding: FragmentGalleryBinding
        get() {
            return _binding!!
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGalleryBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mQueue = Volley.newRequestQueue(requireContext())

        recyclerView = binding.imagesList
        recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        imageGalleryAdapter =
            ImageGalleryAdapter(requireContext(), requireActivity(), photosList)

        photosList.clear()
        parsePhotos()
    }

    private fun parsePhotos() {
        val url: String = BuildConfig.SERVER_URL + "photos/?client_id=" + BuildConfig.API_TOKEN

        val request = JsonArrayRequest(Request.Method.GET, url, null, { response ->
            try {
                for (i in 0 until response.length()) {
                    val item = Photo()
                    val jsonObject: JSONObject = response.getJSONObject(i)

                    val jsonImage: JSONObject = jsonObject.getJSONObject("urls")
                    val imageUrl: String = jsonImage.getString("full")

                    val jsonUser: JSONObject = jsonObject.getJSONObject("user")
                    val author: String = jsonUser.getString("name")

                    val title: String = jsonUser.getString("username")

                    item.imageId = i
                    item.imageUrl = imageUrl
                    item.imageAuthor = author
                    item.imageTitle = title

                    photosList.add(item)
                }

                recyclerView.adapter = imageGalleryAdapter

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, { error -> error.printStackTrace() })

        mQueue.add(request)
    }
}