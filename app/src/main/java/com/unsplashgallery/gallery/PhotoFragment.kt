package com.unsplashgallery.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.unsplashgallery.databinding.FragmentPhotoBinding

class PhotoFragment: Fragment() {

    private lateinit var mImageView: ImageView
    private lateinit var mImageTitle: TextView
    private lateinit var mImageAuthor: TextView
    private lateinit var mBackButton: AppCompatImageButton

    private var _binding: FragmentPhotoBinding? = null
    private val binding: FragmentPhotoBinding
        get() {
            return _binding!!
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPhotoBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mImageTitle = binding.imageTitle
        mImageView = binding.imageView
        mImageAuthor = binding.imageAuthor
        mBackButton = binding.backButton

        arguments?.let {
            val photoInfo = it.getStringArrayList("PHOTO_INFO")

            val url: String = photoInfo!!.get(0)
            Glide.with(requireContext())
                .load(url)
                .centerCrop()
                .into(mImageView)

            mImageTitle.text = photoInfo.get(1)
            mImageAuthor.text = photoInfo.get(2)
        }

        mBackButton.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }
}