package com.unsplashgallery.helpers

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.unsplashgallery.MainActivity
import com.unsplashgallery.R
import com.unsplashgallery.model.Photo

class ImageGalleryAdapter(val context: Context, val activity: Activity, val photosList: ArrayList<Photo>) :
    RecyclerView.Adapter<ImageGalleryAdapter.PhotosHolder>() {

    val EMPTY_LIST_TYPE: Int = 0
    val NON_EMPTY_LIST_TYPE: Int = 1

    override fun getItemViewType(position: Int): Int {
        return if (photosList.isEmpty()) {
            EMPTY_LIST_TYPE
        } else {
            NON_EMPTY_LIST_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosHolder {
        val photoView: View
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        if (viewType == EMPTY_LIST_TYPE) {
            photoView = inflater.inflate(R.layout.photo_no_item, parent, false)
        } else {
            photoView = inflater.inflate(R.layout.photo_item, parent, false)
        }

        return PhotosHolder(photoView)
    }

    override fun onBindViewHolder(mainHolder: PhotosHolder, position: Int) {
        if (getItemViewType(position) == EMPTY_LIST_TYPE) {
            return
        }

        val holder = mainHolder
        val photo: Photo = photosList.get(position)

        val url: String = photo.imageUrl
        if (!url.isEmpty()) {
            Glide.with(context)
                .load(url)
                .centerCrop()
                .into(holder.mImageView)
        }

        holder.mPhotoTitle.text = photo.imageTitle
        holder.mPhotoAuthor.text = photo.imageAuthor

    }

    override fun getItemCount(): Int {
        return if (photosList.isEmpty()) {
            1
        } else {
            photosList.size
        }
    }

    inner class PhotosHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        var mPhotoAuthor: TextView = itemView.findViewById(R.id.item_author)
        var mPhotoTitle: TextView = itemView.findViewById(R.id.item_title)
        var mImageView: ImageView = itemView.findViewById(R.id.item_photo)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val photo = photosList.get(position)

                val bundle = Bundle()
                val array = arrayListOf(photo.imageUrl, photo.imageTitle, photo.imageAuthor)

                bundle.putStringArrayList("PHOTO_INFO", array)
                activity.findNavController(R.id.nav_host_fragment).navigate(R.id.navigation_photo, bundle)
            }
        }
    }
}